from django.conf.urls import patterns, include, url
import views

urlpatterns = patterns('',
	url(r'^$', views.index, name='prueba_'),
	url(r'^samplejson/$', views.jsonsample, name='prueba_index'),
	url(r'^params/$', views.params, name='prueba_params'),
	url(r'^paises$', views.list_add_paises, name='prueba_listadd'),
	url(r'^paises/(?P<id_pais>\d+)$', views.get_upd_del_paises, name='prueba_getupddel'),
	url(r'^paises/(?P<id_pais>\d+)/ciudades$', views.get_ciudades_pais, name='prueba_cidpais'),
	url(r'^dobleoutput\.(?P<salida>(xml|json|yaml))$', views.dobleoutput, name='prueba_xmlorjson'),
)
