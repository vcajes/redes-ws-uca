import json
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseNotAllowed, HttpResponseNotFound
from django.views.decorators.csrf import csrf_exempt
from django.core.exceptions import ObjectDoesNotExist
from models import Pais, Ciudad, Persona
from django.core import serializers

@csrf_exempt
def params(request):
	if request.method == "GET":
		p1 = request.GET.get("p1")
		return HttpResponse(unicode(p1), status=200)
	elif request.method == "POST":
		p1 = request.POST.get("p1")
		p2 = request.POST.get("no_existe")
		return HttpResponse(unicode(p1) + " - " + unicode(p2), status=200)
	return HttpResponseNotAllowed(permitted_methods=["GET", "POST"])

def index(request):
	return render(request, "temp_index/index.html", {"data1": "texto random"})

def jsonsample(request):
	return HttpResponse(json.dumps({"hola": "chau"}), content_type="application/json", status=201)

@csrf_exempt
def list_add_paises(request):
	if request.method == "GET":
		paises = Pais.objects.all()
		return HttpResponse(unicode(serializers.serialize('json', paises)))

	elif request.method == "POST":
		nom = request.POST.get("nombre")
		ed = request.POST.get("edad")
		pais = Pais(nombre=nom, descripcion="", independiente=True, edad=ed, website=None)
		pais.save(force_insert=True)
		return HttpResponse(unicode(serializers.serialize('json',[pais, ])), status=201)
	return HttpResponseNotAllowed(permitted_methods=["GET, POST"])

@csrf_exempt
def get_upd_del_paises(request, id_pais):
	try:
		pais = Pais.objects.get(pk=id_pais)
	except ObjectDoesNotExist:
		return HttpResponseNotFound()

	if request.method == "GET":
		return HttpResponse(unicode(serializers.serialize('json', [pais, ])))

	elif request.method == "POST":
		#actualizamos los campos existentes
		if request.POST.has_key("nombre"):
			pais.nombre = unicode(request.POST.get("nombre"))
		if request.POST.has_key("edad"):
			pais.edad = unicode(request.POST.get("edad"))
		pais.save(force_update=True)
		return HttpResponse(unicode(serializers.serialize('json', [pais, ])))

	elif request.method == "DELETE":
		pais.delete()
		return HttpResponse(unicode(serializers.serialize('json', [pais, ])))
	return HttpResponseNotAllowed(permitted_methods=["GET", "POST", "DELETE"])

def get_ciudades_pais(request, id_pais):
	if request.method == "GET":
		ciudades = Ciudad.objects.filter(pais__pk=id_pais)
		return HttpResponse(unicode(serializers.serialize('json', ciudades)))
	return HttpResponseNotAllowed(permitted_methods=["GET"])

def dobleoutput(request, salida):
	if request.method == "GET":
		paises = Pais.objects.all()
		return HttpResponse(unicode(serializers.serialize(salida, paises)))
	return HttpResponseNotAllowed(permitted_methods=["GET"])

