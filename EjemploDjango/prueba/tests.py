import json
from models import Pais
from django.test import Client, TestCase

class PaisesTests(TestCase):
	def setUp(self):
		Pais.objects.create(nombre="Qatar")
		Pais.objects.create(nombre="Chile")

	def test_animal_creation(self):
		c = Client()

		#agregamos un pais y verificamos si se creo
		response = c.post('/prueba/paises', {'nombre': 'Paraguay', 'edad': '200'})
		self.assertEqual(response.status_code, 201)

		#obtenemos la lista de paises
		response = c.get('/prueba/paises')
		self.assertEqual(response.status_code, 200)

		#tienen que haber creados tres paises
		self.assertEqual(len(json.loads(response.content)), 3)

		print json.loads(response.content)[0]["fields"]["nombre"]

