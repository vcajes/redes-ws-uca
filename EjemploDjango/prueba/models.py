from django.db import models

class Pais(models.Model):
	nombre = models.CharField(null=False, max_length=80, unique=True)
	descripcion = models.TextField(default="")
	independiente = models.BooleanField(default=True)
	edad = models.IntegerField(default=0)
	website = models.URLField(null=True)

class Ciudad(models.Model):
	nombre = models.CharField(null=False, max_length=80, unique=True)
	pais = models.ForeignKey(Pais) #clave foranea

class Persona(models.Model):
	nombre = models.CharField(null=False, max_length=50, unique=True)
	apellido = models.CharField(max_length=50, null=True)
	direccion = models.TextField()
	edad = models.IntegerField()
	website = models.URLField(null=True)
	paises_visitados = models.ManyToManyField(Pais) #many to many
	creacion = models.DateField(auto_now_add=True, null=True)
	edicion = models.DateTimeField(auto_now=True, null=True)

