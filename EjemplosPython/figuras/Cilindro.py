
# -*- coding: utf-8 -*-

from math import pi as MATHPI
from figuras.Circulo import Circulo

class Cilindro(Circulo):
	def __init__(self, x, y, radio, altura):
		super(Cilindro, self).__init__(x, y, radio)
		self.altura = altura

	def area(self):
		return 2 * MATHPI * self.radio * (self.altura + self.radio)

	def volumen(self):
		return MATHPI * (self.radio ** 2.0) * self.altura

