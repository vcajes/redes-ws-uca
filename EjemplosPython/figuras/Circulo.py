
# -*- coding: utf-8 -*-

from math import pi as MATHPI

class Circulo(object):
	descripcion = "[SIN DESCRIPCION]"
	autor = "[SIN AUTOR]"

	def __init__(self, x, y, radio):
		self.x = x
		self.y = y
		self.radio = radio

	def area(self):
		return MATHPI * self.radio ** 2.0

	def perimetro(self):
		return 2 * MATHPI * self.radio

