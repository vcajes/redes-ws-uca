
# -*- coding: utf-8 -*-

#el famoso hola mundo
print("Hello World!")

#declaracion de variable de tipo ENTERO
var_int = 10
print("El valor de var1 es " + str(var_int))

#declaracion de variable de tipo PUNTO FLOTANTE
var_float = 778.695
print("El valor de var2 es " + str(var_float))

#declaracion de variable de tipo BOOLEANO
var_bool = True #False
print("El valor de var2 es " + str(var_bool))

#declaracion de variable de tipo STRING
var_string1 = "String 1"
var_string2 = 'String 2'
var_string_unicode = u"String 1"
var_string_multiple = """String
de multiples
lineas"""

print("El valor de var_string1 es " + str(var_string1))
print("El valor de var_string2 es " + str(var_string2))
print("El valor de var_string_unicode es " + str(var_string_unicode))
print("El valor de var_string_multiple es " + str(var_string_multiple))


#Casteos de tipos de datos
var_cast_int = int("5")
var_suma = 4 + var_cast_int
print("El valor de var_suma es " + str(var_suma))

var_cast_int = float("3.5")
var_mult = 1.5 * var_cast_int
print("El valor de var_mult es " + str(var_mult))

str1 = u"Asunción" #debemos anteponer la u para acentos
print(str1)

#operaciones con cadenas
#concatenacion
print("hola" + " " + "%s %s" % ("mundo", 'feliz'))

#split
split_str = "7,8,9".split(",")
print("El split de la cadena split_str es: %s" % split_str)

#start with | ends with
print("pelota tata".startswith("pel"))
print("pelota tata".endswith("pel"))


#estructuras de control
a = 7
b = None
if a == 7 and b is None:
    print("a=7 & b=None --> TRUE")

if a != 7 or b is not None:
    print("a=7 O b=None --> TRUE")
elif not True:
    print("Nunca va entrar aca")
else:
    print("Se imprime el caso por DEFAULT")

while a > 3:
    a -= 1
    if a == 6:
        pass
    elif a == 5:
        continue
    elif a == 4:
        break
    print("El valor de a: %d" % a)

#desde x=3 hasta 9-1 paso 2
for x in range(3,9,2):
    print "El valor de x: %d" % x


#listas en python
lista = [1, 2, 3]
print(lista)

lista.append(4)
print(lista)

val = lista[1]
print("El valor de la posicion 1 es %d" % val)

val = lista.pop()
print("El valor obtenido es %d" % val)
print(lista)

val = lista.reverse()
print(lista)

print("La lista tiene %d elementos." % len(lista))

#iterar lista - Forma 1
for i in range(len(lista)):
    print("El valor de la posicion es: lista[%d] = %d" % (i, lista[i]))

#iterar lista - Forma 2 (mas simple)
for value in lista:
    print("El valor de la iteracion es: value = %d" % value)

#arreglos n dimensionales
lista2 = [[1, 'a', 98, 'h'], [78, "HOLA"]]
vector = [x*2 for x in range(10)]
matriz = [[y*x for y in range(3)] for x in range(3)]

print(lista2[0])
print(lista2[1][1])
print(lista2)
print(vector)
print(matriz)

#recorrer arreglos
for x in range(len(lista2)):
    for y in range(len(lista2[x])):
        print("V: %s" % str(lista2[x][y]))

import json

#diccionarios
abc = dict() #Forma2--> abc = {}
abc["a1"] = 15
abc["a2"] = 'pelota'
abc["d9"] = "se va a borrar"
del abc["d9"] #borrar elemento del dic
print(json.dumps(abc))

dic2 = {"b1": 18, "b2": "clock"}
print(json.dumps(dic2))

print dic2.has_key("b1")
print dic2.has_key("c4")

#recorrer todos los (key, values)
for key, value in dic2.iteritems():
    print key, value


#definicion de funcion
def fibonacci(n):
    a, b = 0, 1
    while a < n:
        print a,
        a, b = b, a+b
    return a, b

#llamada a la funcion
v1, v2 = fibonacci(1000)
print ""
print("Valor 1: %d" % v1)
print("Valor 2: %d" % v2)

#definicion de procedimiento
def proced1(data):
    print("El valor de 'data' es: %s" % data)
    if data == "salir":
        return
    print("No se retorna valor/es. %s" % data)

#llamada a la funcion
proced1("salir")


#manejo de errores
while True:
    try:
        x = int(raw_input("Ingrese un numero: "))
        print "El numero ingresado es x = %d" % x
        break
    except ValueError as e1:
        print e1.message
        print "Error, no ingresaste un numero. Vuelve a intentarlo."
    #except: #captura cualquier otra excepcion
    except Exception as e2: #captura cualquier otra excepcion
        print e2.message







