
# -*- coding: utf-8 -*-

from figuras.Circulo import Circulo
from figuras.Cilindro import Cilindro

cir = Circulo(10, 20, 5.6)
cir.descripcion = "Circulo 1"
print cir.descripcion
print cir.area()

cil = Cilindro(10, 20, 30, 7.6)
cil.descripcion = "Cilindro 1"
print cil.descripcion
print cil.area()
