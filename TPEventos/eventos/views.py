
# -*- coding: utf-8 -*-

import time
import json
import hashlib
from django.conf import settings
from models import Usuario, Accesstoken, Eventos
from django.http import HttpResponse, HttpResponseNotAllowed
from django.views.decorators.csrf import csrf_exempt

@csrf_exempt
def login(request):
	if request.method == "POST":
		usr = request.POST.get("usuario", None)
		passw = request.POST.get("contrasena", None)
		if usr and passw:
			# buscar un usuario en la base de datos
			try:
				user = Usuario.objects.filter(usuario=usr).filter(contrasena=passw).get()
			except:
				user = None
			if user:
				token = hashlib.sha256(usr + str(time.time())).hexdigest()
				seconds_to_expire = getattr(settings, "TOKEN_EXPIRE")
				exp = int(time.time()) + seconds_to_expire
				accesstoken = Accesstoken(token=token, usuario=user, expiracion=exp)
				accesstoken.save(force_insert=True)
				return HttpResponse(json.dumps({
					"access_token": token
				}), status=200, content_type="application/p=json")
			else:
				return HttpResponse("Usuario/Contrasena invalido.", status=400, content_type="text/plain")
		else:
			return HttpResponse("Faltan parametros de entrada.", status=400, content_type="text/plain")
	return HttpResponseNotAllowed(permitted_methods=["POST"])


@csrf_exempt
def usuarios(request):
	if request.method == "POST":
		nombre = request.POST.get("nombre", None)
		apellido = request.POST.get("apellido", None)
		mail = request.POST.get("mail", None)
		usuario = request.POST.get("usuario", None)
		contrasena = request.POST.get("contrasena", None)

		if nombre and apellido and mail and usuario and contrasena:
			user = Usuario(
				nombre=nombre, apellido=apellido, mail=mail,
				usuario=usuario, contrasena=contrasena
			)
			user.save(force_insert=True)

			return HttpResponse(json.dumps({
				"id": user.pk,
				"nombre": user.nombre,
				"apellido": user.apellido,
				"mail": user.mail,
				"usuario": user.usuario,
				"contrasena": user.contrasena,
			}), status=201, content_type="application/json")
		return HttpResponse("Faltan parametros de entrada.", status=400, content_type="text/plain")
	return HttpResponseNotAllowed(permitted_methods=["GET"])


@csrf_exempt
def eventos(request):
	if request.method == "GET":
		atoken = request.GET.get("token", None)
		if atoken:
			accesstoken = Accesstoken.objects.filter(token=atoken).get()
			if accesstoken:
				if accesstoken.is_expired():
					return HttpResponse("Sesion expirada.", status=400, content_type="text/plain")
				else:
					lista_eventos = Eventos.objects.filter(usuario=accesstoken.usuario)
					return HttpResponse(json.dumps(
						[{
							"id": evento.pk,
							"nombre": evento.nombre,
							"fecha": evento.fecha,
							"usuario": evento.usuario.pk
						} for evento in lista_eventos]
					), status=200, content_type="application/p=json")
			else:
				return HttpResponse("Sesion invalida.", status=400, content_type="text/plain")
		else:
			return HttpResponse("Faltan parametros de entrada.", status=400, content_type="text/plain")
	return HttpResponseNotAllowed(permitted_methods=["GET"])