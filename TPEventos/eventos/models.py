
# -*- coding: utf-8 -*-

import time
from django.db import models

class Usuario(models.Model):
	nombre = models.CharField(null=False, max_length=80)
	apellido = models.CharField(null=False, max_length=80)
	mail = models.EmailField(null=False, unique=True, max_length=80)
	usuario = models.CharField(null=False, unique=True, max_length=80)
	contrasena = models.CharField(null=False, max_length=255)
	habilitado = models.BooleanField(default=True)

class Eventos(models.Model):
	nombre = models.CharField(null=False, max_length=80)
	fecha = models.IntegerField(null=False, default=0)
	usuario = models.ForeignKey(Usuario, default=None)

class Accesstoken(models.Model):
	token = models.CharField(null=False, max_length=256)
	usuario = models.ForeignKey(Usuario, null=False)
	expiracion = models.IntegerField(null=False, default=0)

	def is_expired(self):
		if int(time.time()) > self.expiracion:
			return True
		return False