
# -*- coding: utf-8 -*-

from django.conf.urls import patterns, url
import views

urlpatterns = patterns('',
	url(r'^usuarios$', views.usuarios, name='usuarios'),
	url(r'^login$', views.login, name='login'),

	# eventos
	url(r'^eventos$', views.eventos, name='eventos'),
)
