
# -*- coding: utf-8 -*-

import json
from django.test import Client, TestCase

######################################################
# LOS NOMBRES DE SUS TESTS DEBEN COMENZAR CON: test_ #
######################################################

class EventsTests(TestCase):
	def setUp(self):
		Client().post('/usuarios', {"nombre": "victor", "apellido": "cajes",
			"mail": "vcajes@gmail.com", "usuario": "vcajes",
			"contrasena": "12345"
		})

		Client().post('/usuarios', {"nombre": "miguel", "apellido": "prieto",
			"mail": "jmpr.py@gmail.com", "usuario": "jmpr",
			"contrasena": "12345"
		})

	def test_registro_correcto(self):
		response = Client().post('/usuarios', {
			"nombre": "luis",
			"apellido": "gaona",
			"mail": "lgaona@gmail.com",
			"usuario": "lgaona",
			"contrasena": "12345"
		})
		self.assertEqual(response.status_code, 201)

	def test_registro_incorrecto_falta_parametros(self):
		response = Client().post('/usuarios', {
			"nombre": "juan",
			"apellido": "perez",
		})
		self.assertIsNot(response.status_code, 201)

	def test_login_correcto(self):
		response = Client().post('/login', {
			"usuario": "vcajes", "contrasena": "12345",
		})
		self.assertEqual(response.status_code, 200)
		parsed = json.loads(response.content)
		self.assertTrue(parsed.has_key("access_token"))
		self.assertEqual(len(parsed["access_token"]), 64)

	def test_login_incorrecto_credenciales(self):
		response = Client().post('/login', {
			"usuario": "vcajes", "contrasena": "password_incorrecto",
		})
		self.assertEqual(response.status_code, 400)

	def test_logout_correcto(self):
		response = Client().post('/login', {
			"usuario": "vcajes", "contrasena": "12345",
		})
		self.assertEqual(response.status_code, 200)
		parsed = json.loads(response.content)
		response = Client().post('/logout', {"token": parsed["access_token"]})
		self.assertEqual(response.status_code, 200)

	def test_logout_incorrecto_token(self):
		response = Client().post('/logout', {"token": "token_invalido"})
		self.assertEqual(response.status_code, 400)

	def test_crear_evento_correcto(self):
		response = Client().post('/login', {
			"usuario": "vcajes", "contrasena": "12345",
		})
		token = (json.loads(response.content))["access_token"]

		response = Client().post('/eventos', {
			"token": token,
			"nombre": "evento 1",
			"datetime": 1234567896
		})
		self.assertEqual(response.status_code, 201)
		self.assertGreaterEqual((json.loads(response.content))["id"], 1)

	def test_crear_evento_incorrecto_datos(self):
		response = Client().post('/login', {
			"usuario": "vcajes", "contrasena": "12345",
		})
		token = (json.loads(response.content))["access_token"]

		response = Client().post('/eventos', {
			"token": token,
			"nombre": "evento 1",
			"datetime": "datetime_incorrecto"
		})
		self.assertEqual(response.status_code, 400)

	def test_crear_evento_incorrecto_token_inexistente(self):
		response = Client().post('/eventos', {
			"token": "token_inexistente",
			"nombre": "evento 2",
			"datetime": 1234567896
		})
		self.assertEqual(response.status_code, 403)

	def test_listar_eventos_correcto(self):
		response = Client().post('/login', {
			"usuario": "vcajes", "contrasena": "12345",
		})
		token = (json.loads(response.content))["access_token"]

		response = Client().post('/eventos', {
			"token": token,
			"nombre": "evento 3",
			"datetime": 1234567896
		})
		self.assertEqual(response.status_code, 201)

		response = Client().get('/eventos', {"token": token})

		self.assertEqual(response.status_code, 200)
		eventos = json.loads(response.content)
		self.assertGreaterEqual(len(eventos), 1)

	def test_listar_eventos_incorrecto_token_inexistente(self):
		response = Client().get('/eventos', {"token": "token_inexistente"})
		self.assertEqual(response.status_code, 403)

	def test_borrar_evento_correcto(self):
		response = Client().post('/login', {
			"usuario": "vcajes", "contrasena": "12345",
		})
		token = (json.loads(response.content))["access_token"]

		response = Client().post('/eventos', {
			"token": token,
			"nombre": "evento 3",
			"datetime": 1234567896
		})
		self.assertEqual(response.status_code, 201)

		evento = json.loads(response.content)
		self.assertGreaterEqual(int(evento["id"]), 1)
		self.assertEqual(evento["nombre"], "evento 3")

		response = Client().delete('/eventos/%s?token=%s' % (str(evento["id"]), token), {})
		self.assertEqual(response.status_code, 200)

	def test_borrar_evento_incorrecto_inexistente(self):
		response = Client().post('/login', {
			"usuario": "vcajes", "contrasena": "12345",
		})
		token = (json.loads(response.content))["access_token"]

		response = Client().delete('/eventos/99999999?token=%s' % str(token), {})
		self.assertEqual(response.status_code, 403)

	def test_borrar_evento_incorrecto_token_inexistente(self):
		response = Client().post('/login', {
			"usuario": "vcajes", "contrasena": "12345",
		})
		token = (json.loads(response.content))["access_token"]

		response = Client().post('/eventos', {
			"token": token,
			"nombre": "evento 5",
			"datetime": 1234567896
		})
		self.assertEqual(response.status_code, 201)

		evento = json.loads(response.content)
		self.assertGreaterEqual(int(evento["id"]), 1)
		self.assertEqual(evento["nombre"], "evento 5")

		response = Client().delete('/eventos/%s?token=%s' % (str(evento["id"]), "token_inexistente"), {})
		self.assertEqual(response.status_code, 403)

	def test_actualizar_evento_correcto(self):
		response = Client().post('/login', {
			"usuario": "vcajes", "contrasena": "12345",
		})
		token = (json.loads(response.content))["access_token"]

		response = Client().post('/eventos', {
			"token": token,
			"nombre": "evento 30",
			"datetime": 1234567896
		})
		self.assertEqual(response.status_code, 201)

		evento = json.loads(response.content)
		self.assertGreaterEqual(int(evento["id"]), 1)

		response = Client().post('/eventos/%s' % str(evento["id"]), {
			"token": token,
			"nombre": "evento 90",
			"datetime": 1234567896
		})
		self.assertEqual(response.status_code, 200)
		evento = json.loads(response.content)
		self.assertEqual(evento["nombre"], "evento 90")
		self.assertEqual(evento["datetime"], 1234567896)

	def test_actualizar_evento_incorrecto_datetime(self):
		response = Client().post('/login', {
			"usuario": "vcajes", "contrasena": "12345",
		})
		token = (json.loads(response.content))["access_token"]

		response = Client().post('/eventos', {
			"token": token,
			"nombre": "evento 31",
			"datetime": 1234567896
		})
		self.assertEqual(response.status_code, 201)

		evento = json.loads(response.content)
		self.assertGreaterEqual(int(evento["id"]), 1)

		response = Client().post('/eventos/%s' % str(evento["id"]), {
			"token": token,
			"nombre": "evento 91",
			"datetime": "datetime_incorrecto"
		})
		self.assertEqual(response.status_code, 400)

	def test_actualizar_evento_incorrecto_permisos(self):
		response = Client().post('/login', {
			"usuario": "vcajes", "contrasena": "12345",
		})
		token = (json.loads(response.content))["access_token"]

		response = Client().post('/eventos/99999', {
			"token": token,
			"nombre": "evento 92",
			"datetime": 1234567896
		})
		self.assertEqual(response.status_code, 403)

	def test_listar_invitaciones_correcto(self):
		response = Client().post('/login', {
			"usuario": "vcajes", "contrasena": "12345"
		})
		tokenV = (json.loads(response.content))["access_token"]

		response = Client().post('/login', {
			"usuario": "jmpr", "contrasena": "12345"
		})
		tokenM = (json.loads(response.content))["access_token"]

		response = Client().post('/eventos', {
			"token": tokenV,
			"nombre": "evento 6",
			"datetime": 1234567896
		})
		self.assertEqual(response.status_code, 201)
		evento = json.loads(response.content)

		response = Client().post('/invitaciones', {
			"token": tokenV,
			"id_evento": int(evento["id"]),
			"username": "jmpr"
		})
		self.assertEqual(response.status_code, 201)

		response = Client().get('/invitaciones', {"token": tokenM})
		self.assertEqual(response.status_code, 200)
		invitaciones = json.loads(response.content)
		self.assertGreaterEqual(len(invitaciones), 1)

	def test_listar_invitaciones_incorrecto_token_inexistente(self):
		response = Client().get('/invitaciones', {"token": "token_inexistente"})
		self.assertEqual(response.status_code, 403)

	def test_enviar_invitaciones_correcto(self):
		response = Client().post('/login', {
			"usuario": "vcajes", "contrasena": "12345"
		})
		token = (json.loads(response.content))["access_token"]

		response = Client().post('/eventos', {
			"token": token,
			"nombre": "evento 9",
			"datetime": 1234567896
		})
		self.assertEqual(response.status_code, 201)
		evento = json.loads(response.content)

		response = Client().post('/invitaciones', {
			"token": token,
			"id_evento": int(evento["id"]),
			"username": "jmpr"
		})
		self.assertEqual(response.status_code, 201)

	def test_enviar_invitaciones_incorrecto_evento_inexistente(self):
		response = Client().post('/login', {
			"usuario": "vcajes", "contrasena": "12345"
		})
		token = (json.loads(response.content))["access_token"]

		response = Client().post('/invitaciones', {
			"token": token,
			"id_evento": 999999,
			"username": "jmpr"
		})
		self.assertEqual(response.status_code, 403)

	def test_aceptar_invitacion_correcto(self):
		response = Client().post('/login', {
			"usuario": "vcajes", "contrasena": "12345"
		})
		tokenV = (json.loads(response.content))["access_token"]

		response = Client().post('/login', {
			"usuario": "jmpr", "contrasena": "12345"
		})
		tokenM = (json.loads(response.content))["access_token"]

		response = Client().post('/eventos', {
			"token": tokenV,
			"nombre": "evento 69",
			"datetime": 1234567896
		})
		evento = json.loads(response.content)

		response = Client().post('/invitaciones', {
			"token": tokenV,
			"id_evento": int(evento["id"]),
			"username": "jmpr"
		})
		self.assertEqual(response.status_code, 201)

		response = Client().get('/invitaciones', {"token": tokenM})
		inv = (json.loads(response.content))[0]

		response = Client().post('/aceptaciones/%s' % str(inv["id"]), {"token": tokenM})
		self.assertEqual(response.status_code, 200)

	def test_aceptar_invitacion_incorrecto_estado(self):
		response = Client().post('/login', {
			"usuario": "vcajes", "contrasena": "12345"
		})
		tokenV = (json.loads(response.content))["access_token"]

		response = Client().post('/login', {
			"usuario": "jmpr", "contrasena": "12345"
		})
		tokenM = (json.loads(response.content))["access_token"]

		response = Client().post('/eventos', {
			"token": tokenV,
			"nombre": "evento 70",
			"datetime": 1234567896
		})
		evento = json.loads(response.content)

		response = Client().post('/invitaciones', {
			"token": tokenV,
			"id_evento": int(evento["id"]),
			"username": "jmpr"
		})
		self.assertEqual(response.status_code, 201)

		response = Client().get('/invitaciones', {"token": tokenM})
		inv = (json.loads(response.content))[0]

		response = Client().post('/aceptaciones/%s' % str(inv["id"]), {"token": tokenM})
		self.assertEqual(response.status_code, 200)

		response = Client().post('/aceptaciones/%s' % str(inv["id"]), {"token": tokenV})
		self.assertEqual(response.status_code, 400)

	def test_rechazar_invitacion_correcto(self):
		response = Client().post('/login', {
			"usuario": "vcajes", "contrasena": "12345"
		})
		tokenV = (json.loads(response.content))["access_token"]

		response = Client().post('/login', {
			"usuario": "jmpr", "contrasena": "12345"
		})
		tokenM = (json.loads(response.content))["access_token"]

		response = Client().post('/eventos', {
			"token": tokenV,
			"nombre": "evento 71",
			"datetime": 1234567896
		})
		evento = json.loads(response.content)

		response = Client().post('/invitaciones', {
			"token": tokenV,
			"id_evento": int(evento["id"]),
			"username": "jmpr"
		})
		self.assertEqual(response.status_code, 201)

		response = Client().get('/invitaciones', {"token": tokenM})
		inv = (json.loads(response.content))[0]

		response = Client().post('/rechazos/%s' % str(inv["id"]), {"token": tokenM})
		self.assertEqual(response.status_code, 200)

	def test_rechazar_invitacion_incorrecto_permiso(self):
		response = Client().post('/login', {
			"usuario": "vcajes", "contrasena": "12345"
		})
		tokenV = (json.loads(response.content))["access_token"]

		response = Client().post('/login', {
			"usuario": "jmpr", "contrasena": "12345"
		})
		tokenM = (json.loads(response.content))["access_token"]

		response = Client().post('/eventos', {
			"token": tokenV,
			"nombre": "evento 72",
			"datetime": 1234567896
		})
		evento = json.loads(response.content)

		response = Client().post('/invitaciones', {
			"token": tokenV,
			"id_evento": int(evento["id"]),
			"username": "jmpr"
		})
		self.assertEqual(response.status_code, 201)

		response = Client().get('/invitaciones', {"token": tokenM})
		inv = (json.loads(response.content))[0]

		response = Client().post('/rechazos/%s' % str(inv["id"]), {"token": tokenM})
		self.assertEqual(response.status_code, 200)

		response = Client().post('/rechazos/%s' % str(inv["id"]), {"token": tokenV})
		self.assertEqual(response.status_code, 400)
