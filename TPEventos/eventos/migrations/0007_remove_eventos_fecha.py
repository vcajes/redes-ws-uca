# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('eventos', '0006_auto_20150306_1814'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='eventos',
            name='fecha',
        ),
    ]
