# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('eventos', '0002_usuario_habilitado'),
    ]

    operations = [
        migrations.CreateModel(
            name='Accesstoken',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('token', models.CharField(max_length=256)),
                ('expiracion', models.DateTimeField()),
                ('usuario', models.ForeignKey(to='eventos.Usuario')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
