# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('eventos', '0007_remove_eventos_fecha'),
    ]

    operations = [
        migrations.AddField(
            model_name='eventos',
            name='fecha',
            field=models.IntegerField(default=0),
            preserve_default=True,
        ),
    ]
