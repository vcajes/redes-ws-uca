# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('eventos', '0004_eventos'),
    ]

    operations = [
        migrations.AddField(
            model_name='eventos',
            name='usuario',
            field=models.ForeignKey(default=None, to='eventos.Usuario'),
            preserve_default=True,
        ),
    ]
