
# -*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url
import eventos.urls

urlpatterns = patterns('',
	 url(r'^', include(eventos.urls)),
)
