
# -*- coding: utf-8 -*-

#EJERCICIO 1
def factorial(n):
	val = 1
	while n > 1:
		val *= n
		n -= 1
	return val
try:
	num = int(raw_input("Ingrese el numero:\t"))
	if num > 0:
		print("factorial(%d) = %d" % (num, factorial(num)))
	else:
		raise ValueError("El numero ingresado es menor a 1")
except ValueError as e:
	print e.message
	print("ERROR: Debes ingresar un numero entero positivo")


#EJERCICIO 2
from math import sin, cos
print("El coseno de %f es %f" % (3.0, cos(3.0)))
print("El seno de %f es %f" % (3.0, sin(3.0)))


#EJERCICIO 3
cant = int(raw_input("Ingrese la cantidad de elementos:\t"))
lista = [int(raw_input("Ingrese el valor:\t")) for x in range(cant)]
print("El promedio del vector es: %.4f" % (float(sum(lista)) / len(lista)))
